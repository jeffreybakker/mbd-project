#!/usr/bin/python3
#
# Given the ENV variable PYSPARK_PYTHON=python3:
# time spark-submit main.py 2> /dev/null
#
# Or in the cluster:
# time spark-submit \
#   --master yarn \
#   --deploy-mode cluster \
#   --conf spark.dynamicAllocation.maxExecutors=10 \
#   --executor-memory 4G \
#   --num-executors 8 \
#   --executor-cores 4 \
#   main.py 2> /dev/null
# 2019-Okt + 2019-Nov => 3mins, 29sec
# Alle data => 34 mins, 37sec
from typing import Dict

from pyspark import SparkContext
from pyspark.sql import SparkSession

# Configure the spark context
sc = SparkContext(appName='ecommerce')
sc.setLogLevel("ERROR")

# Define application data
source = '/user/s1836943/ecommerce'  # /2019-*.csv'
dest = '/user/s1836943/ecommerce/results'

# Get dataframe
print('Loading data...')
spark = SparkSession.builder.getOrCreate()
df = spark.read.csv(source, header=True)
df.printSchema()

# 1. Convert data to RDD
# 2. Prepare lookup array with { [key: product_id]: row }
# 3. Make sure there is only one of every `product_id`
products = df \
    .rdd \
    .map(lambda row: (row['product_id'], row)) \
    .reduceByKey(lambda a, b: a)


##
# RQ1: Which products are the most often bought?
##

print('Processing RQ1...')
# 1. Only consider all purchase events
# 2. Convert to RDD
# 3. Prepare word count based on `product_id`
# 4. Count occurrences of each `product_id`
# 5. Inject product data into word count
# 6. Prepare output format
# 7. Sort the result by amount bought in descending order
rq1 = df \
    .filter(df.event_type == 'purchase') \
    .rdd \
    .map(lambda row: (row['product_id'], 1)) \
    .reduceByKey(lambda a, b: a + b) \
    .leftOuterJoin(products) \
    .map(lambda row: (row[1][0],                    # Amount bought
                      row[0],                       # Product ID
                      row[1][1]['category_id'],     # Category ID
                      row[1][1]['category_code'],   # Category code
                      row[1][1]['brand'],           # Brand
                      row[1][1]['price']            # Price
                      )) \
    .sortBy(lambda row: row[0], ascending=False)

print('Saving RQ1...')
rq1 \
    .map(lambda row: ';'.join(map(lambda item: str(item), row)), True) \
    .saveAsTextFile(f'{dest}/rq1')


##
# RQ2: Which (types of) products are the most often added to shopping cart, but not bought?
##

def combine_dicts(a: Dict[str, int], b: Dict[str, int]) -> Dict[str, int]:
    res = {}
    for k, v in a.items():
        if k in b:
            res[k] = v + b[k]
        else:
            res[k] = v

    for k, v in b.items():
        if k in res:
            continue
        res[k] = v

    return res


print('Processing RQ2...')
# 1. We only care about events with type `cart` or `purchase`
# 2. Convert the DataFrame to an RDD
# 3. Prepare word count for alternatively bought products
# 4. Perform word count for alternatively bought products
# 5. Prepare for selection of most popular alternative
# 6. Perform word count for most popular alternative selection
# 7. Prepare selection of most popular alternative
# 8. Select the most popular alternative
rq2 = df \
    .filter((df.event_type == 'cart') | (df.event_type == 'purchase')) \
    .rdd \
    .map(lambda row: (row['user_session'], {row['product_id']: 0 if row['event_type'] == 'cart' else 1})) \
    .reduceByKey(combine_dicts) \
    .flatMap(lambda session: [(k,
                               {p: q for p, q in session[1].items() if q > 0})
                              for k, v in session[1].items() if v == 0]) \
    .reduceByKey(combine_dicts) \
    .flatMap(lambda product: [(product[0], (v, k, v)) for k, v in product[1].items()]) \
    .reduceByKey(lambda a, b: (a[0] + b[0], a[1] if a[2] > b[2] else b[1], max(a[2], b[2])))

# 1. Prepare data for detail lookup
# 2. Perform lookup
# 3. Prepare output format
# 4. Sort the result by amount removed from cart in descending order
rq2_removed = rq2 \
    .map(lambda row: (row[0], row[1][0])) \
    .leftOuterJoin(products) \
    .map(lambda row: (row[0],                       # Product ID
                      row[1][0],                    # Amount removed from cart
                      row[1][1]['category_id'],     # Category ID
                      row[1][1]['category_code'],   # Category code
                      row[1][1]['brand'],           # Brand
                      row[1][1]['price']            # Price
                      )) \
    .sortBy(lambda row: row[1], ascending=False)

print('Saving RQ2...')
rq2_removed \
    .map(lambda row: ';'.join(map(lambda item: str(item), row)), True) \
    .saveAsTextFile(f'{dest}/rq2')


##
# RQ3: What trends can be identified in the purchasing process?
##
# - Is it possible to explain why these trends exist using the dataset?


print('Processing RQ3...')
# 1. Prepare data for detail lookup
# 2. Perform lookup
# 3. Prepare output format
# 4. Sort the result by amount removed from cart in descending order
rq3_alt = rq2 \
    .map(lambda row: (row[1][1], (row[0], row[1][0], row[1][2]))) \
    .leftOuterJoin(products) \
    .map(lambda row: (row[1][0][0],                 # Product ID
                      row[1][0][1],                 # Amount removed from cart
                      row[0],                       # Alternative product ID
                      row[1][1]['category_id'],     # Alternative category ID
                      row[1][1]['category_code'],   # Alternative category code
                      row[1][1]['brand'],           # Alternative brand
                      row[1][1]['price']            # Alternative price
                      )) \
    .sortBy(lambda row: row[1], ascending=False)

# 1. We only care about purchase events
# 2. Convert to RDD
# 3. Select data and set granularity of timestamp to 1.0 hours
# 4. Discard `None` categories
# 5. Prepare for word count on amount bought in timeframe
# 6. Perform word count on amount bought in timeframe
# 7. Extract encoded data (IDs)
# 8. Correct extraction
# 9. Sort on date (ascending)
rq3_timestamps = df \
    .filter(df.event_type == 'purchase') \
    .rdd \
    .map(lambda row: (row['category_code'], row['event_time'].split(':')[0] + ':00:00 UTC')) \
    .filter(lambda row: row[0] != None) \
    .map(lambda row: (row[0] + '|' + row[1], 1)) \
    .reduceByKey(lambda a, b: a + b) \
    .map(lambda row: (row[0].split('|'), row[1])) \
    .map(lambda row: (row[0][0], row[0][1], row[1])) \
    .sortBy(lambda row: row[1], ascending=True)

print('Saving RQ3...')
rq3_alt \
    .map(lambda row: ';'.join(map(lambda item: str(item), row)), True) \
    .saveAsTextFile(f'{dest}/rq3-1')

rq3_timestamps \
    .map(lambda row: ';'.join(map(lambda item: str(item), row)), True) \
    .saveAsTextFile(f'{dest}/rq3-2')
