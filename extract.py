#!/usr/bin/python3
# Jeffrey Bakker - s1836943
# Danique Lummen - s1853155
# Time needed to execute: 0m17.673s (real: 0m9.554s)
#
# Given the ENV variable PYSPARK_PYTHON=python3:
# time spark-submit extract.py 2> /dev/null
import time

from pyspark import SparkContext

# Configure the spark context
sc = SparkContext(appName='extract')
sc.setLogLevel("ERROR")

# Define application data
source = '/user/s1836943/ecommerce/results/rq3-2'

# Retrieve all files in the given directory
files = sc.wholeTextFiles(source)
res = files \
    .flatMap(lambda file: file[1].split('\n'))

print(f'Extracting {res.count()} lines in 10s...')
for i in range(10):
    print(f'{10-i}s...')
    time.sleep(1)

with open('output', 'w') as f:
    for line in res.collect():
        f.write(line + '\n')
